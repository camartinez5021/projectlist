import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: 'posts',
    loadChildren: () => import('./components/posts/posts.module')
    .then(modulo => modulo.PostsModule)
  },
  {
    path: 'comments',
    loadChildren: () => import('./components/comments/comments.module')
    .then(modulo => modulo.CommentsModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
