export interface IComments {
    length?: number;
    postId: number;
    id: number;
    name: string;
    email: string;
    body: String;
}