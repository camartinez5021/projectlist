import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IComments } from './interface/comments';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CommentsService {

  constructor(private https: HttpClient) { }

  public queryComments(): Observable<IComments[]> {
    return this.https.get<IComments[]>(`${environment.END_POINT}/comments`)
      .pipe(map(res => {
        return res;
      }));
  }

  public getCommentsByName(name: string): Observable<IComments>{
    let params = new HttpParams();
    params = params.append('name', name);
    console.warn('PARAMS ',params);
    return this.https.get<IComments>(`${environment.END_POINT}/comments`,{params: params})
      .pipe(map(res => {
        return res;
      }));
    }
}
