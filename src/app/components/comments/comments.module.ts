import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommentsListComponent } from './comments-list/comments-list.component';
import { CommentsCreateComponent } from './comments-create/comments-create.component';
import { CommentsRoutingModule } from './comments-routing.module';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [CommentsListComponent, CommentsCreateComponent],
  imports: [
    CommonModule,
    CommentsRoutingModule,
    ReactiveFormsModule
  ]
})
export class CommentsModule { }
