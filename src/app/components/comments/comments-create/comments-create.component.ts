import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { IComments } from '../interface/comments';
import { CommentsService } from '../comments.service';

@Component({
  selector: 'app-comments-create',
  templateUrl: './comments-create.component.html',
  styleUrls: ['./comments-create.component.styl']
})
export class CommentsCreateComponent implements OnInit {
  CommentsFormGroup: FormGroup;
  comments: IComments;

  statusSearchName: boolean = false;

  commentsList: IComments[] = [];
  formSearchComments = this.formBuilder.group({
    name: ['']
  });

  constructor(private formBuilder: FormBuilder, private commentsService: CommentsService) { }

  ngOnInit() {
  }


  searchComments() {
   console.warn('Data', this.formSearchComments.value);
    this.commentsService.getCommentsByName(this.formSearchComments.value.name)
    .subscribe(res => {
      console.warn('response comments by name ',res);
      this.comments = res;
      if(res.length > 0){
        this.statusSearchName = false;
      }else {
        this.statusSearchName = true;
      }

});
}
}