import { Component, OnInit } from '@angular/core';
import { CommentsService } from '../comments.service';
import { IComments } from '../interface/comments';

@Component({
  selector: 'app-comments-list',
  templateUrl: './comments-list.component.html',
  styleUrls: ['./comments-list.component.styl']
})
export class CommentsListComponent implements OnInit {

  public commentsList: IComments[];

  constructor(private commentsService: CommentsService) { }

  ngOnInit() {
    this.commentsService.queryComments()
    .subscribe(res => {
      this.commentsList = res;
    });
  }

}
