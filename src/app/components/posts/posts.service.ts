import { Injectable } from '@angular/core';
import { HttpClient, HttpParams} from '@angular/common/http';
import { Observable } from 'rxjs';
import { IPosts } from './interface/posts';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  constructor(private https: HttpClient) { }

  public queryPost(): Observable<IPosts[]> {
    return this.https.get<IPosts[]>(`${environment.END_POINT}/posts`)
      .pipe(map(res => {
        return res;
      }));
  }

  public getPostByTitle(title: string): Observable<IPosts>{
    let params = new HttpParams();
    params = params.append('title', title);
    console.warn('PARAMS ',params);
    return this.https.get<IPosts>(`${environment.END_POINT}/posts`,{params: params})
      .pipe(map(res => {
        return res;
      }));
    }


}
