import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { IPosts } from '../interface/posts';
import { PostsService } from '../posts.service';

@Component({
  selector: 'app-posts-create',
  templateUrl: './posts-create.component.html',
  styleUrls: ['./posts-create.component.styl']
})
export class PostsCreateComponent implements OnInit {
  PostsFormGroup: FormGroup;
  posts: IPosts;

  statusSearchTitle: boolean = false;

  formSearchPosts = this.formBuilder.group({
    title: ['']
  });

  constructor(private formBuilder: FormBuilder, private postsService: PostsService) { }

  ngOnInit() {
  }

  searchPosts() {
    console.warn('Data', this.formSearchPosts.value);
    this.postsService.getPostByTitle(this.formSearchPosts.value.title)
    .subscribe(res => {
      console.warn('response posts by title ',res);
      this.posts = res;
      if(res.length > 0){
        this.statusSearchTitle = false;
      }else {
        this.statusSearchTitle = true;
      }
    });
  }

}
