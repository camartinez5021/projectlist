import { Component, OnInit } from '@angular/core';
import { PostsService } from '../posts.service';
import { IPosts } from '../interface/posts';

@Component({
  selector: 'app-posts-list',
  templateUrl: './posts-list.component.html',
  styleUrls: ['./posts-list.component.styl']
})
export class PostsListComponent implements OnInit {

  public postsList: IPosts[];

  constructor(private postsService: PostsService) { }

  ngOnInit() {
    this.postsService.queryPost()
    .subscribe(res => {
      this.postsList = res;
    });
  }

}
